(asdf:defsystem #:stumpwm-mullvad			
  :serial t
  :description "Stumpwm Mullvad cli integration"
  :author "Fermin MF"
  :license "GPLv3"
  :depends-on (:stumpwm :alexandria :cl-ppcre :uiop)
  :components ((:file "package")
               (:file "main")))

