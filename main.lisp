(in-package #:stumpwm-mullvad)

(defvar *mullvad-executable* "mullvad")

(defvar *mullvad-current-tunnel* nil)

(defvar *mullvad-locations* nil)

(defvar *mullvad-formatters-alist*
  '((#\V  fmt-mullvad-status)))

(defvar *mullvad-modeline-fmt* "VPN: %V"
  "Mullvad modeline formatter, using ")

(defstruct mullvad-tunnel
  type ip channel server)

(defstruct mullvad-location
  name ip-list sublocation main abbrev)

(defun mullvad-send-command (command)
  (stumpwm::run-shell-command
   (format nil "~A ~a" *mullvad-executable* command) t))

(defgeneric make-status (status type))

;; ("Tunnel" "status:" "Connected" "to" "OpenVPN" "IP" "over" "UDP")
(defmethod make-status ((status string) (type (eql :openvpn)))
  (let* ((status-list (cl-ppcre:split "\\s+" status))
	 (server (sixth status-list)))
    (and status-list
	 (make-mullvad-tunnel
	  :type :openvpn
	  :ip (sixth status-list)
	  :channel (car (last status-list))
	  :server (and server
		       (find-location server type))))))

;; "Connected to SERVER in LOCATION, COUNTRY "
(defmethod make-status ((status string) (type (eql :wireguard)))
  (let* ((status-list (cl-ppcre:split "\\s+" status))
	 (server (third status-list)))
    (and status-list
	 (multiple-value-bind (location ip) (find-location server :wireguard)
	   (make-mullvad-tunnel
	    :type :wireguard
	    :ip ip
	    :channel nil
	    :server location)))))

(defun mullvad-status ()
  (let ((status (mullvad-send-command "status")))
    (cond
      ((search "OpenVPN" status :test #'string-equal)
       (make-status status :openvpn))
      ((not (or (search "Disconnected" status :test #'string-equal)
		(search "Disconnecting" status :test #'string-equal)))
       (make-status status :wireguard))
      (t nil))))

(defun mullvad-locations ()
  "Transform the input from the relay list command to a list of structs of
type `mullvad-location'."
  (labels ((main-location-p (location)
	     (and (not (emptyp location)) (upper-case-p (elt location 0))
		  (char-equal (alexandria:last-elt location) #\))))
	   (sublocation-p (location)
	     (and (not (emptyp location)) (upper-case-p (elt location 0))
		  (not (char-equal (alexandria:last-elt location) #\)))))
	   (location-name (location)
	     (subseq location 0 (1- (position #\( location))))
	   (location-abbrev (location number)
	     (let ((first (format nil "[a-z]{~a}" number))
		   (second (format nil "\\([a-z]{~a}\\)" number)))
	       (cl-ppcre:scan-to-strings first (cl-ppcre:scan-to-strings second  location)))))

    (let* ((locations (mullvad-send-command "relay list"))
	   (location-list (uiop:split-string locations :separator '(#\Tab #\Newline)))
	   (main-locations nil))

      (loop for location in location-list
	    with current-main = nil
	    with current-sub = nil
	    do (cond
		 ;; Australia (au)
		 ((main-location-p location)
		  (let ((name (location-name location))
			(abbrev (location-abbrev location 2)))
		    (and current-main (push current-main main-locations))
		    (setf current-main (make-mullvad-location
					:name name :main t :abbrev abbrev))))

		 ;; Secaucus, NJ (uyk) @ 40.78954°N, -74.05650°W
		 ((sublocation-p location)
		  (let ((name (location-name location))
			(abbrev (location-abbrev location 3)))
		    (setf current-sub (make-mullvad-location
				       :name name :abbrev abbrev))
		    (push
		     current-sub
		     (mullvad-location-sublocation current-main))))

		 ;; us237-wireguard (162.218.210.18, 2607:fcd0:ccc0:1d02::b37f) - WireGuard, hosted by Quadranet
		 ((not (emptyp location))
		  (let* ((location-list (cl-ppcre:split "\\s+" location))
			 (name (first location-list))
			 (ips (second location-list)))
		    (push (cons name ips) (mullvad-location-ip-list current-sub)))))
	    finally (push current-main main-locations))
      (nreverse main-locations))))

(defun load-locations ()
  (setf *mullvad-locations* (mullvad-locations)))

(defun load-tunnel ()
  (setf *mullvad-current-tunnel* (mullvad-status)))

(defun mullvad-set-location (main-location sublocation)
  (mullvad-send-command (format nil "relay set location ~a ~a"
				main-location
				sublocation)))

(defgeneric find-location (ip type)
  (:documentation "Find the mullvad location from an ip."))

(defmethod find-location ((ip string) (type (eql :openvpn)))
  (loop for location in *mullvad-locations*
	for sublocations = (mullvad-location-sublocation location)
	with port-pos = (position #\: ip )
	with ip-no-port = (or (and port-pos (subseq ip 0 port-pos )) ip)
	when (loop for subl in sublocations
		   for ip-list = (mullvad-location-ip-list subl)
		   return (loop for (name . current-ip) in ip-list
				when (cl-ppcre:scan ip-no-port current-ip )
				return t))
	return location))

;; In this case, we return a cons with the location AND the IP
(defmethod find-location ((server string) (type (eql :wireguard)))
  (let ((location-ip (loop with ip = nil
			   for location in *mullvad-locations*
			   for sublocations = (mullvad-location-sublocation location)
			   when (loop for subl in sublocations
				      for ip-list = (mullvad-location-ip-list subl)
				      return (loop for (name . current-ip) in ip-list
						   when (string-equal name server)
						   return (prog1 t
							    (setf ip current-ip))))
			   return (cons location ip))))
    (values (car location-ip)
	    (remove #\,(remove #\( (cdr location-ip))))))

(defun current-location ()
  (and (mullvad-status)
       (format nil "~A, ~a"
	       (mullvad-location-name
		(mullvad-tunnel-server *mullvad-current-tunnel*))
	       (mullvad-location-abbrev
		(mullvad-tunnel-server *mullvad-current-tunnel*)))))

(defun general-information ()
  "Return a string of general information about the current Mullvad
  status."
  (format nil "~a ~a ~a Location: ~a"
	  (mullvad-send-command "status")
	  (mullvad-send-command "lan get")
	  (mullvad-send-command "auto-connect get")
	  (current-location)))

(defun fmt-mullvad-status ()
  (if (mullvad-status) "On" "Off"))

(defun mullvad-modeline (ml)
  (declare (ignore ml))
  (stumpwm::format-expand *mullvad-formatters-alist*
			  *mullvad-modeline-fmt* ))

(stumpwm::add-screen-mode-line-formatter #\V 'mullvad-modeline)

(stumpwm::defcommand  mullvad-connect () ()
  "Connect to the last mullvad location"
  (mullvad-send-command "connect")
  (stumpwm::message (mullvad-send-command "status")))

(stumpwm::defcommand  mullvad-disconnect () ()
  "Disconnect from Mullvad"
  (mullvad-send-command "disconnect")
  (stumpwm::message (mullvad-send-command "status")))

(stumpwm::defcommand  mullvad-general-information () ()
  "Display general information about the current mullvad status."
  (unless *mullvad-locations*
    (load-locations))
  (load-tunnel)
  (stumpwm::message (general-information)))


(stumpwm::defcommand mullvad-select-location () ()
  "Select a location from *mullvad-locations* and connect to it."
  (unless (mullvad-status)
    (stumpwm:message "Mullvad tunnel disconnected.")
    (return-from mullvad-select-location 0))

  (unless *mullvad-locations*
    (load-locations))

  (let* ((main-location-name (car
			      (stumpwm:select-from-menu
			       (stumpwm:current-screen)
			       (mapcar #'mullvad-location-name *mullvad-locations*)
			       "Main location: ")))
	 (main-location (find main-location-name *mullvad-locations*
			      :key #'mullvad-location-name :test #'string-equal))
	 (sublocation-name  (car (stumpwm:select-from-menu
				  (stumpwm:current-screen)
				  (mapcar #'mullvad-location-name
					  (mullvad-location-sublocation main-location))
				  "Sublocation: ")))
	 (sublocation (find sublocation-name (mullvad-location-sublocation main-location)
			    :key #'mullvad-location-name :test #'string-equal)))
    (mullvad-set-location
     (mullvad-location-abbrev main-location)
     (mullvad-location-abbrev sublocation))))

(stumpwm::defcommand mullvad-update-locations () ()
  "Update Mullvad locations."
  (stumpwm::message (mullvad-send-command "relay update")))

(stumpwm::defcommand mullvad-current-location () ()
  "Show current Mullvad location"
  (unless *mullvad-locations*
    (load-locations))
  (load-tunnel)
  (if (mullvad-status)
      (stumpwm::message (current-location))
      (stumpwm::message "Disconnected.")))

(stumpwm::defcommand mullvad-menu () ()
  "Main Mullvad menu for command navigation."
  (let* ((options '(("general-information" mullvad-general-information)
		    ("connect" mullvad-connect)
		    ("disconnect" mullvad-disconnect)
		    ("current-location" mullvad-current-location)
		    ("select-location" mullvad-select-location)
		    ("update-locations" mullvad-update-locations)))
	 (selected-option (stumpwm:select-from-menu
			   (stumpwm:current-screen)
			   (mapcar #'car options)
			   "Mullvad menu"))
	 (selected-function (find (if (listp selected-option)
				      (car selected-option)
				      selected-option)
				  options :key #'car :test #'string-equal)))
    (and selected-function (funcall (second selected-function)))))
