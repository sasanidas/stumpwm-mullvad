;;;; package.lisp

(defpackage #:stumpwm-mullvad
  (:use #:cl #:alexandria)
  (:export
   ;; Customizable variables
   #:*mullvad-executable*
   #:*mullvad-current-tunnel*
   #:*mullvad-locations*

   ;; Status functions
   #:mullvad-status
   #:mullvad-locations

   ;; Load functions
   #:load-locations
   #:load-tunnel))

